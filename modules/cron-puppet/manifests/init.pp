class cron-puppet {
    case $::osfamily {
		'debian': {
			file { 'post-hook':
				ensure  => file,
				path    => '/etc/puppet/.git/hooks/post-merge',
				source  => 'puppet:///modules/cron-puppet/debian-post-merge',
				mode    => 0755,
				owner   => root,
				group   => root,
			}
			cron { 'puppet-apply':
				ensure => present,
				command => "cd /etc/puppet ; /usr/bin/git pull",
				user    => root,
				minute  => '*/30',
				require => File['post-hook'],
			}
		}
		default: {
			fail("Unsupported osfamily: ${::osfamily}. Submit an issue at https://gitlab.com/callahma/uni-puppet/issues/new to gain support")
		}
		
	}
}